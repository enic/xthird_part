编译流程：
[windows]
1.执行PCbuild/get_externals.bat下载依赖
2.直接使用sln开始编译。或者使用build_env.bat设置环境，在使用build.bat编译
3.编译结果在PCbuild目录下的win32或者amd64目录。
4.拷贝全部dll和exe到安装目录
5.在安装目录新建lib目录，然后拷贝root/lib目录下的全部文件到安装目录的lib文件.另外exe同级目录中的*.pyd文件也需要放到lib中
6.PC目录下，有pyconfig.h需要拷贝到sdk的include目录中
7.默认是64为的，但是可以在build.bat文件中修改platf=x86


源码目录介绍：
Include ：该目录下包含了Python提供的所有头文件，如果用户需要自己用C或C++来编写自定义模块扩展Python，那么就需要用到这里提供的头文件。
Lib ：该目录包含了Python自带的所有标准库，Lib中的库都是用Python语言编写的。
Modules ：该文件夹中包含了所有用C语言编写的模块，比如ramdom，cStringIO等，Modules中的模块是那些对速度要求非常严格的模块。而有一些对速度没有太严格要求的模块，比如os，就是用Python编写，并且放在Lib目录下
Parser ：Parser目录中包含了Python解释器中的Scanner和Parser部分，即对Python源代码进行词法分析和语法分析的部分。除了这些，Parser目录下还包含了一些有用的工具，这些工具能够根据Python语言的语法自动生成Python语言的词法和语法分析器，与YACC非常类似。
Objects ：该目录中包含了所有Python的内建对象，包括整数，list，dict等；同时，该目录还包括了Python在运行时需要的所有的内部使用对象的实现
Python ：该目录下包含了Python解释器中的Compiler和执行引擎部分，是Python运行的核心所在。
PCBuild ：包含了Visual Studio 2003工程文件，研究Python源代码就从这里开始，实际使用中我使用的是VS 2015。







