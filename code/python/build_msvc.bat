cd Python-3.10.0/PCbuild
call build.bat -p=x86 -c=Release -m
::build.bat -p=x86 -c=Debug 
::build.bat -p=x64 -c=Release
::build.bat -p=x64 -c=Debug


cd ../../../../
mkdir prebuild
cd prebuild
mkdir python
cd python
::mkdir include
::mkdir lib
::mkdir bin

::pwd
cd ../../code/python/Python-3.10.0/
::pwd
xcopy PCbuild\win32\*.exe  ..\..\..\prebuild\python\win\bin  /I /Y
xcopy PCbuild\win32\*.dll  ..\..\..\prebuild\python\win\bin  /I /Y
xcopy PCbuild\win32\*.pdb  ..\..\..\prebuild\python\win\bin  /I /Y
xcopy PCbuild\win32\*.pyd  ..\..\..\prebuild\python\win\bin\lib  /I /Y
xcopy Lib                  ..\..\..\prebuild\python\win\bin\lib  /I /Y /E

xcopy PCbuild\win32\*.lib  ..\..\..\prebuild\python\win\lib  /I /Y

xcopy Include  ..\..\..\prebuild\python\win\include  /I /Y /E
xcopy PC\pyconfig.h  ..\..\..\prebuild\python\win\include  /I /Y 

cd ..
