#include "minitrace.h"

#ifdef _WIN32
#include <windows.h>
#define usleep(x) Sleep(x/1000)
#else
#include <unistd.h>
#endif
#include <thread>

void c() {
	MTR_SCOPE("", "c()");
	usleep(10000);
}

void b() {
	MTR_SCOPE("", "b()");
	usleep(20000);
	c();
	usleep(10000);
}

void a() {
	MTR_SCOPE("", "a()");
	usleep(20000);
	b();
	usleep(10000);
}

void d() {
	MTR_SCOPE("", __FUNCTION__);
	usleep(20000);
}

void e() {
	MTR_SCOPE("", __FUNCTION__);
	usleep(30000);
	d();
}

void f() {
	usleep(10000);
}

void worker_thread()
{
	MTR_META_THREAD_NAME("custom_work_thread");
	a();
	usleep(10000);
	d();
	usleep(10000);
	e();
	a();
	usleep(10000);
	d();
	usleep(10000);
	e();
	a();
	usleep(10000);
	d();
	usleep(10000);
	e();
}

int main() {
	int i;
	mtr_init("trace.json");
	mtr_register_sigint_handler();

	std::thread work_thread(worker_thread);

	MTR_META_PROCESS_NAME("minitrace_test");
	MTR_META_THREAD_NAME("main thread");

	int long_running_thing_1;
	int long_running_thing_2;

	MTR_START("background", "long_running", &long_running_thing_1);
	MTR_START("background", "long_running", &long_running_thing_2);

	MTR_COUNTER("main", "greebles", 3);
	MTR_BEGIN("main", "outer");
	usleep(80000);
	for (i = 0; i < 3; i++) {
		MTR_BEGIN("main", "inner");
		usleep(40000);
		MTR_END("main", "inner");
		usleep(10000);
		MTR_COUNTER("main", "greebles", 3 * i + 10);
	}
	MTR_STEP("background", "long_running", &long_running_thing_1, "middle step");
	usleep(80000);
	MTR_END("main", "outer");
	MTR_COUNTER("main", "greebles", 0);

	usleep(10000);
	a();

	usleep(50000);
	MTR_INSTANT("main", "the end");
	usleep(10000);
	MTR_FINISH("background", "long_running", &long_running_thing_1);
	MTR_FINISH("background", "long_running", &long_running_thing_2);

	work_thread.join();
	mtr_flush();
	mtr_shutdown();
	return 0;
}
