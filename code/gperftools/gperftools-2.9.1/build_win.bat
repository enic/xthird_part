mkdir build_msvc
cd build_msvc

:: -Dgperftools_build_minimal=False只有linux支持
cmake -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTING=False -A Win32 -T host=x86 -DOBJCOPY_EXECUTABLE=""  -DCMAKE_INSTALL_PREFIX="../../../../prebuild/gperftools/win" ../
cmake  --build . --config Debug --target INSTALL
cmake  --build . --config Release --target INSTALL

cd ..

xcopy bin\*.bin  ..\..\..\prebuild\gperftools\win\bin\  /I /Y
xcopy bin\*.dll  ..\..\..\prebuild\gperftools\win\bin\  /I /Y
xcopy bin\*.pdb  ..\..\..\prebuild\gperftools\win\bin\  /I /Y
xcopy bin\*.exe  ..\..\..\prebuild\gperftools\win\bin\  /I /Y


