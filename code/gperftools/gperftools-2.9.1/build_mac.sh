mkdir cmake_target_debug
cd cmake_target_debug

cmake -DCMAKE_BUILD_TYPE=Debug -Dgperftools_build_benchmark=False -DGPERFTOOLS_BUILD_STATIC=True -DBUILD_TESTING=False -DCMAKE_INSTALL_PREFIX="../../../../prebuild/gperftools/mac" ../
make
make install

cd ..

mkdir cmake_target_release
cd cmake_target_release

cmake -DCMAKE_BUILD_TYPE=Release -Dgperftools_build_benchmark=False -DGPERFTOOLS_BUILD_STATIC=True -DBUILD_TESTING=False -DCMAKE_INSTALL_PREFIX="../../../../prebuild/gperftools/mac" ../
make
make install

cd ..

