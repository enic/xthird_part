mkdir build_mac
cd build_mac

cmake -DCMAKE_BUILD_TYPE=Debug -DLIBUV_BUILD_TESTS=False -DCMAKE_INSTALL_PREFIX="../../../../prebuild/libuv/mac" ../
make
make install

cmake -DCMAKE_BUILD_TYPE=Release -DLIBUV_BUILD_TESTS=False -DCMAKE_INSTALL_PREFIX="../../../../prebuild/libuv/mac" ../
make
make install

cd ..



