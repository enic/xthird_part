
build_dir="build_mac"
echo building minitrace in directory: $build_dir
if [ ! -d "$build_dir" ]; then
    echo "create directory: $build_dir"
    mkdir -p "$build_dir"
else
    echo "directory already exists: $build_dir"
fi

cd $build_dir

cmake -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX="../../../prebuild/minitrace/mac" ../
make all
make install

cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="../../../prebuild/minitrace/mac" ../
make all
make install

cd ..
