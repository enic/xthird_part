mkdir build_linux
cd build_linux

cmake -DCMAKE_BUILD_TYPE=Debug -DARG_NB_STATIC=ON -DCMAKE_INSTALL_PREFIX="../../../../prebuild/nanobind/linux" ../
make
make install

cmake -DCMAKE_BUILD_TYPE=Release -DARG_NB_STATIC=ON -DCMAKE_INSTALL_PREFIX="../../../../prebuild/nanobind/linux" ../
make
make install

cd ..



