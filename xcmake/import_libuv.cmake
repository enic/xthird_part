#
#
# export:
# 头文件目录: XTP_LIBUV_INC_PATH
# 库自动添加：libuv_static

message("-- xthird_part: import libuv")
#message(${CMAKE_CURRENT_SOURCE_DIR})
#message(${CMAKE_BINARY_DIR})

SET(XTHIRD_PART_PREBUILD_DIR ${CMAKE_CURRENT_SOURCE_DIR}/xthird_part/prebuild)

# prebuild base路径
SET(TARGET_BASE_PATH "unset")
IF(${XCMAKE_TARGET_OS_WIN})
    SET(TARGET_BASE_PATH ${XTHIRD_PART_PREBUILD_DIR}/libuv/win)
ELSEIF(${XCMAKE_TARGET_OS_LINUX})
    SET(TARGET_BASE_PATH ${XTHIRD_PART_PREBUILD_DIR}/libuv/linux)
ELSEIF(${XCMAKE_TARGET_OS_MAC})
    SET(TARGET_BASE_PATH ${XTHIRD_PART_PREBUILD_DIR}/libuv/mac)
ELSE()
    MESSAGE("-- xthird_part: unsupport target os")
ENDIF()

IF(${TARGET_BASE_PATH} STREQUAL "unset")
    MESSAGE("-- xthird_part: setup faild !")
ELSE()
    SET(TARGET_LIB_PATH ${TARGET_BASE_PATH}/lib)
    SET(TARGET_BIN_PATH ${TARGET_BASE_PATH}/bin)
    SET(TARGET_INC_PATH ${TARGET_BASE_PATH}/include)
ENDIF()

#message(${XCMAKE_TARGET_OS_MAC} ${TARGET_BASE_PATH})
#UNSET(_tmp_debug_lib CACHE)
#UNSET(_tmp_release_lib CACHE)
IF(${XCMAKE_TARGET_OS_WIN})
    find_library(_tmp_release_lib uv_a.lib ${TARGET_BASE_PATH}/lib NO_DEFAULT_PATH)
    find_library(_tmp_debug_lib uv_a_d.lib ${TARGET_BASE_PATH}/lib NO_DEFAULT_PATH)
ELSE()
    find_library(_tmp_release_lib libuv_a.a ${TARGET_BASE_PATH}/lib NO_DEFAULT_PATH)
    find_library(_tmp_debug_lib libuv_a_d.a ${TARGET_BASE_PATH}/lib NO_DEFAULT_PATH)
ENDIF()
#MESSAGE("ddddd" ${_tmp_release_lib} ${_tmp_debug_lib})

add_library(libuv_static STATIC IMPORTED)
set_target_properties(libuv_static PROPERTIES
IMPORTED_LOCATION_DEBUG ${_tmp_debug_lib}
IMPORTED_LOCATION_RELEASE ${_tmp_release_lib}
IMPORTED_LOCATION_RELWITHDEBINFO ${_tmp_release_lib}
IMPORTED_LOCATION_MINSIZEREL ${_tmp_release_lib}
)

SET(XTP_LIBUV_INC_PATH ${TARGET_INC_PATH})

MESSAGE("---xthird_part: setup libuv_static: ")
MESSAGE("   XTP_LIBUV_INC_PATH: " ${XTP_LIBUV_INC_PATH})
MESSAGE("   DEBUG lib: " ${_tmp_debug_lib})
MESSAGE("   RELASE & RELWITHDEBINFO lib: " ${_tmp_release_lib})

# 加载libuv
