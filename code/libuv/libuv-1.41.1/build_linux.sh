mkdir build_linux
cd build_linux

cmake -DCMAKE_BUILD_TYPE=Debug -DLIBUV_BUILD_TESTS=False -DCMAKE_INSTALL_PREFIX="../../../../prebuild/libuv/linux" ../
make
make install

cmake -DCMAKE_BUILD_TYPE=Release -DLIBUV_BUILD_TESTS=False -DCMAKE_INSTALL_PREFIX="../../../../prebuild/libuv/linux" ../
make
make install

cd ..



