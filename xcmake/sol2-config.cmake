# coding: utf8
# author: enic
# export:
# include_dir: SOL2_INC_DIR
# libs:

MESSAGE(STATUS "[xthird_part] import sol2-config.cmake")

if (SOL2_FOUND)
    message("---: sol2 already found")
    message("---: SOL2_INC_DIR=${SOL2_INC_DIR}")
    return()
endif()

# 给定的boost目录
MESSAGE("---: SOL2_ROOT:" ${SOL2_ROOT})

# 当前文件所在目录路径
get_filename_component(SOL2_CONFIG_DIR "${CMAKE_CURRENT_LIST_FILE}" DIRECTORY)
MESSAGE("---: SOL2_CONFIG_DIR:" ${SOL2_CONFIG_DIR})

# 默认的boost目录
get_filename_component(DEFAULT_SOL2_SEARCH_DIR "${SOL2_CONFIG_DIR}/../prebuild/sol2/" ABSOLUTE)
MESSAGE("---: DEFAULT_SOL2_SEARCH_DIR: " ${DEFAULT_SOL2_SEARCH_DIR})

find_path(SOL2_INC_DIR sol
    HINTS ${SOL2_ROOT}/include
    PATH_SUFFIXES
    PATHS
        ${DEFAULT_XBOOST_SEARCH_DIR}/linux/include
        ${DEFAULT_XBOOST_SEARCH_DIR}/mac/include
        ${DEFAULT_XBOOST_SEARCH_DIR}/win/include
    NO_DEFAULT_PATH # 禁止默认地址搜索
)
if (XBOOST_INC_DIR)
    MESSAGE("---:found XBOOST_INC_DIR: " ${XBOOST_INC_DIR})
else()
    # 默认从project_root/xthird_part/ 下搜索，外部可以提供 XBOOST_ROOT搜索
    MESSAGE(SEND_ERROR "---:can not find XBOOST_INC_DIR")
    SET(XBOOST_FOUND FALSE)
    return()
endif()

############## xboost::filesystem
find_library(XBOOST_FILESYSTEM_LIB
    NAMES libboost_filesystem.a libboost_filesystem.lib #libboost_filesystem.dylib
    HINTS ${XBOOST_ROOT}/lib
    PATH_SUFFIXES
    PATHS
        ${DEFAULT_XBOOST_SEARCH_DIR}/linux/lib
        ${DEFAULT_XBOOST_SEARCH_DIR}/mac/lib
        ${DEFAULT_XBOOST_SEARCH_DIR}/win/lib
    NO_DEFAULT_PATH
)
find_library(XBOOST_FILESYSTEM_D_LIB
    NAMES libboost_filesystem_d.a libboost_filesystem_d.lib #libboost_filesystem.dylib
    HINTS ${XBOOST_ROOT}/lib
    PATH_SUFFIXES
    PATHS
        ${DEFAULT_XBOOST_SEARCH_DIR}/linux/lib
        ${DEFAULT_XBOOST_SEARCH_DIR}/mac/lib
        ${DEFAULT_XBOOST_SEARCH_DIR}/win/lib
    NO_DEFAULT_PATH
)
if (XBOOST_FILESYSTEM_LIB)
    MESSAGE("---:found XBOOST_FILESYSTEM_LIB: " ${XBOOST_FILESYSTEM_LIB})
    add_library(xboost::filesystem STATIC IMPORTED)
    set_target_properties(xboost::filesystem PROPERTIES
        IMPORTED_LOCATION ${XBOOST_FILESYSTEM_LIB}
        IMPORTED_LOCATION_DEBUG ${XBOOST_FILESYSTEM_D_LIB}
    )
else()
    # 默认从project_root/xthird_part/ 下搜索，外部可以提供 XBOOST_ROOT搜索
    MESSAGE(SEND_ERROR "---:can not find XBOOST_FILESYSTEM_LIB")
endif()
################################################################################

############## xboost::stacktrace
find_library(XBOOST_STACKTRACE_LIB
    NAMES libboost_stacktrace_basic.a libboost_stacktrace_basic.lib
    HINTS ${XBOOST_ROOT}/lib
    PATH_SUFFIXES
    PATHS
        ${DEFAULT_XBOOST_SEARCH_DIR}/linux/lib
        ${DEFAULT_XBOOST_SEARCH_DIR}/mac/lib
        ${DEFAULT_XBOOST_SEARCH_DIR}/win/lib
    NO_DEFAULT_PATH
)
find_library(XBOOST_STACKTRACE_D_LIB
    NAMES libboost_stacktrace_basic_d.a libboost_stacktrace_basic_d.lib
    HINTS ${XBOOST_ROOT}/lib
    PATH_SUFFIXES
    PATHS
        ${DEFAULT_XBOOST_SEARCH_DIR}/linux/lib
        ${DEFAULT_XBOOST_SEARCH_DIR}/mac/lib
        ${DEFAULT_XBOOST_SEARCH_DIR}/win/lib
    NO_DEFAULT_PATH
)
if (XBOOST_STACKTRACE_LIB)
    MESSAGE("---:found XBOOST_STACKTRACE_LIB: " ${XBOOST_STACKTRACE_LIB})
    add_library(xboost::stracktrace STATIC IMPORTED)
    set_target_properties(xboost::stracktrace PROPERTIES
        IMPORTED_LOCATION ${XBOOST_STACKTRACE_LIB}
        IMPORTED_LOCATION_DEBUG ${XBOOST_STACKTRACE_D_LIB}
    )
else()
    # 默认从project_root/xthird_part/ 下搜索，外部可以提供 XBOOST_ROOT搜索
    MESSAGE(SEND_ERROR "---:can not find XBOOST_STACKTRACE_LIB")
endif()
################################################################################





if (NOT TARGET xboost::filesystem)
    message(WARNING "---: xboost::filesystem is not ready !")
endif()
if (NOT TARGET xboost::stracktrace)
    message(WARNING "---: xboost::stracktrace is not ready !")
endif()

SET(XBOOST_FOUND TRUE)

# 导出
mark_as_advanced(XBOOST_FOUND XBOOST_INC_DIR)
